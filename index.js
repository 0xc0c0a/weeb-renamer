require('dotenv').config();

const assert = require('assert').strict;
const Discord = require('discord.js');
const slugify = require('slugify');
const channelId = require('./channel-id.json');
const channelNames = require('./channel-names.json');

const client = new Discord.Client();

client.on('ready', async () => {
    const channel = await client.channels.fetch(channelId);
    assert(channel != null);

    await channel.send('i am awive UwU');

    const newName = slugify(channelNames[new Date().getDay()]);
    await channel.setName(newName);

    client.destroy();
});

const token = process.env.WEEB_RENAMER_TOKEN;
assert(token != null);

client.login(token);
